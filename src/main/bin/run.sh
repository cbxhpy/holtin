#!/bin/bash
source /etc/profile
. /etc/init.d/functions

JAR_FILE="@name@.jar"
PID_FILE="application.pid"
JAVA_OPTS="$JAVA_OPTS @javaOpts@"
JAVA_HOME="$JDK8_HOME"

### 警告信息
notice(){
    echo "用法：$0 {start|stop|restart}"
}

### 启动
start(){
    if [ -e $PID_FILE ];then
        action "$0 is running" /bin/true
    else
        #screen -dmS "${JAR_FILE/%.jar/}-$RANDOM" $JAVA_HOME/bin/java $JAVA_OPTS -jar $JAR_FILE
        nohup $JAVA_HOME/bin/java $JAVA_OPTS -jar $JAR_FILE >>logs/console.log 2>&1 &
        sleep 15
        stat=$?
        if [ $stat -eq 0 ];then
            action "$0 is running" /bin/true
        else 
            action "启动失败..." /bin/false
        fi
    fi
}

### 停止
stop(){
    if [ ! -e $PID_FILE ];then
        action "$PID_FILE 不存在，停止失败" /bin/false
    elif [ ! -s $PID_FILE ];then
        action "$PID_FILE 文件为空，停止失败" /bin/false
    else
        cat $PID_FILE | xargs kill
        sleep 5
        action "$0 is stopped" /bin/true
    fi
}



if [ $# -eq 1 ];then
  case $1 in
    "start")
        start
        ;;
    "stop")
        stop
        ;;
    "restart")
        stop
        start
        ;;
    *)
        notice
        ;;
    esac  
else
    notice
fi

