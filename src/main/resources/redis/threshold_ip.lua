
local function trim(thresholdIpKey, timestamp, listLength, timeout)
    local opIndex = listLength / 2
    for i = -1, -opIndex do
        local opTime = tonumber(redis.call('lindex', thresholdIpKey, i))
        -- 没有元素
        if (opTime == nil) then
            return
        end
        if os.time() - opTime > timeout then
            redis.call('rpop', thresholdIpKey)
        end
    end
end
local function thresholdIp()
    local thresholdIpKey = tostring(KEYS[1])
    local timestamp = tonumber(ARGV[1])
    local totalTimeLimit = tonumber(ARGV[2])
    local timeout = tonumber(ARGV[3])

    local listLength = tonumber(redis.call('llen', thresholdIpKey))
    if (listLength == nil) then
        listLength = 0
    end
    if listLength < totalTimeLimit then
        redis.call('lpush', thresholdIpKey, timestamp)
        return true
    else
        local earliestTime = tonumber(redis.call('lindex', thresholdIpKey, -1))
        if (earliestTime == nil) then
            earliestTime = 0
        end
        if timestamp - earliestTime < timeout then
            return false
        else
            trim(thresholdIpKey, timestamp, listLength, timeout)
            return thresholdIp()
        end
    end

end

return thresholdIp()