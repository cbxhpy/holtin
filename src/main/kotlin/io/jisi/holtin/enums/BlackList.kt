package io.jisi.holtin.enums

import java.util.Date


/**
 * 黑名单，白名单，可疑名单
 * Created by sunpeak on 2016/8/6.
 */
class BlackList {

    /**
     * 维度
     */
    var dimension: EnumDimension? = null

    /**
     * 类型
     */
    var type: EnumType? = null

    /**
     * 值
     */
    var value: String? = null

    /**
     * 时间
     */
    var time: Date? = null

    /**
     * 详情
     */
    var detail: String? = null

    /**
     * 维度枚举
     */
    enum class EnumDimension {
        MOBILE,
        IP,
        DEVICEID
    }

    /**
     * 类型枚举
     */
    enum class EnumType {
        BLACK,
        WHITE,
        TEMP
    }

}
