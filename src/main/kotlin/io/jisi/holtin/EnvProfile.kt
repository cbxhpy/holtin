package io.jisi.holtin

import org.springframework.core.env.Environment
import org.springframework.core.env.Profiles

/**
 * 应用运行环境名称。
 *
 * @author Kevin Zou (kevinz@weghst.com)
 */
enum class EnvProfile(val profile: String) {

    /**
     * 开发环境。
     */
    DEV("dev"),
    /**
     * 集成测试环境。
     */
    INTEGRATION_TEST("integration-test"),
    /**
     * 测试环境。
     */
    TEST("test"),
    /**
     * 稳定的生产环境。
     */
    PROD("prod"), ;

    /**
     * 环境是否活跃。
     */
    fun active(env: Environment) = env.acceptsProfiles(Profiles.of(profile))
}