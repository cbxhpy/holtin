package io.jisi.holtin.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.web.servlet.config.annotation.EnableWebMvc

/**
 * 程序入口。
 *
 * @author Kevin Zou (kevinz@weghst.com)
 */
@EnableWebMvc
@SpringBootApplication(
        scanBasePackages = ["io.jisi.holtin"],
        exclude = [
            MongoAutoConfiguration::class,
            MongoDataAutoConfiguration::class
        ]
)
@EnableConfigurationProperties()
class Main {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<Main>(*args)
        }
    }
}