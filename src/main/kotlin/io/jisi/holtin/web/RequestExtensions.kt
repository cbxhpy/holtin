package io.jisi.holtin.web

import io.zhudy.kitty.domain.Sort
import io.zhudy.kitty.web.mvc.queryInt
import io.zhudy.kitty.web.mvc.queryString
import io.jisi.holtin.web.qo.PageQo
import io.jisi.holtin.web.qo.TimeQo
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.paramOrNull

/**
 * 返回时间参数。
 */
fun ServerRequest.time(): TimeQo {
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    val startTime = LocalDateTime.parse(this.queryString("start_time"), formatter)
    val endTime = LocalDateTime.parse(this.queryString("end_time"), formatter)
    return TimeQo(startTime, endTime)
}

/**
 * 返回分页参数。
 */
fun ServerRequest.page(): PageQo {
    val page = this.queryInt("page")
    val size = this.queryInt("size")
    return PageQo(page, size)
}

/**
 * 返回排序参数。
 */
fun ServerRequest.sort(): Sort {
    return Sort.parse(this.paramOrNull("sort"))
}

/**
 * 公共的包装参数对象。
 */
fun ServerRequest.sharedParam() = SharedParam(this)

class SharedParam(private val request: ServerRequest) {

    fun pageQo() = request.page()

    fun timeQo() = request.time()

    fun sortQo() = request.sort()

//    val pageQo: PageQo = request.page()

//    val timeQo: TimeQo = request.time()
//
//    val sortQo: Sort = request.sort()
}