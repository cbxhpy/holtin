package io.jisi.holtin.web

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTDecodeException
import com.auth0.jwt.exceptions.SignatureVerificationException
import io.jisi.holtin.domain.PermissionRequest
import io.jisi.holtin.domain.Requirement
import io.jisi.holtin.service.RoleService
import io.jisi.holtin.web.v1.RiskController
import io.zhudy.kitty.auth.UserContext
import io.zhudy.kitty.biz.BizCodeException
import io.zhudy.kitty.biz.PubBizCodes
import io.zhudy.kitty.web.mvc.traceId
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.function.RouterFunction
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router

/**
 * 路由配置。
 *
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Configuration(proxyBeanMethods = false)
class Routers(
        private val jwtAlgorithm: Algorithm,
        private val roleService: RoleService,
        private val riskController: RiskController
) {

    @Bean
    fun corsRouter() = router { OPTIONS("/**") { ok().build() } }

    @Bean
    fun mainRouter(
    ): RouterFunction<ServerResponse> = router {
        "/api/v1".nest {
            HEAD("/") { ok().build() }
            "risk".nest {
                GET("", riskController::req)
            }
        }
    }

    /**
     * 用户认证校验。
     */
    private fun security(next: (ServerRequest) -> ServerResponse): (ServerRequest) -> ServerResponse = { request ->
        checkAccessToken(request)
        next(request)
    }

    /**
     * 用户权限校验。
     */
    private fun security(
            requirement: Requirement,
            next: (ServerRequest) -> ServerResponse
    ): (ServerRequest) -> ServerResponse = { request ->
        val uc = checkAccessToken(request)
        checkAccessAllow(request, requirement, uc)
        next(request)
    }

    private fun checkAccessToken(request: ServerRequest): UserContext {
        val token = request.param("access_token").orElseGet {
            val v = request.headers().header("authorization").firstOrNull()
            if (v != null && v.length >= 7) {
                v.substring(7)
            } else {
                null
            }
        }

        if (token.isNullOrEmpty()) {
            throw BizCodeException(PubBizCodes.C_401, "缺少 access_token 参数")
        }

        val jwt = try {
            JWT.decode(token)
        } catch (e: JWTDecodeException) {
            throw BizCodeException(PubBizCodes.C_401, "错误的 access_token 参数 $token")
        }

        try {
            jwtAlgorithm.verify(jwt)
        } catch (e: SignatureVerificationException) {
            throw BizCodeException(PubBizCodes.C_401, "access_token signature failed")
        }

        val id = try {
            jwt.id.toInt()
        } catch (e: Exception) {
            throw BizCodeException(PubBizCodes.C_401, "错误的 access_token")
        }

        val context = object : UserContext {
            override val attrs: MutableMap<String, String?>
                get() = TODO("not implemented")
            override val id: Int
                get() = id
            override val name: String
                get() = jwt.getClaim("name").asString()
            override val token: String
                get() = token
            override val traceId: String
                get() = request.traceId()
        }
        request.attributes()[UserContext.HTTP_REQUEST_ATTRIBUTE_KEY] = context
        return context
    }

    private fun checkAccessAllow(request: ServerRequest, requirement: Requirement, uc: UserContext) {
        val method = request.methodName()
        val requestUri = request.servletRequest().requestURI
        val r = roleService.checkPermission(PermissionRequest(requirement, method, requestUri), uc)
        if (!r) {
            throw BizCodeException(PubBizCodes.C_403, "你没有权限访问该资源")
        }
    }
}