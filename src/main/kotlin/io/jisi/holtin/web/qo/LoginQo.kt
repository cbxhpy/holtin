package io.jisi.holtin.web.qo

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
data class LoginQo(
    val email: String,
    val password: String
)