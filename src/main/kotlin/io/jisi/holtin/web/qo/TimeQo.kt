package io.jisi.holtin.web.qo

import org.joda.time.LocalDateTime

/**
 * @author obaw
 * @time 2019-08-22 14:07
 * @describe
 */
data class TimeQo(
    val startTime: LocalDateTime,
    val endTime: LocalDateTime
)