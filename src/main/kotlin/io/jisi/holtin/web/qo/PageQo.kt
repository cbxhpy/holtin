package io.jisi.holtin.web.qo

import io.zhudy.kitty.biz.BizCodeException
import io.zhudy.kitty.biz.PubBizCodes

/**
 * @author obaw
 * @time 2019-08-19 15:37
 * @describe 分页
 */
data class PageQo(
    var page: Int = 1,
    var size: Int = 15
) {

    companion object {
        /**
         * 分页最大的记录数。
         */
        var maxSize = 1000
    }

    init {
        if (page <= 0) {
            throw BizCodeException(PubBizCodes.C_999, "page 必须大于 0")
        }
        if (size <= 0) {
            throw BizCodeException(PubBizCodes.C_999, "size 必须大于 0")
        }
        if (size >= maxSize) {
            throw BizCodeException(PubBizCodes.C_999, "size 不能大于 $maxSize")
        }
    }

    val qPage get() = page - 1
}