package io.jisi.holtin.web.v1

import io.jisi.holtin.service.KieService
import io.jisi.holtin.web.event.LoginEvent
import org.springframework.stereotype.Controller
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

@Controller
class RiskController(
        private val kieService: KieService
) {
    fun req(request: ServerRequest): ServerResponse {
        val event = LoginEvent()
        event.operateIp = "192.168.1.1"
        return ServerResponse.ok().body(kieService.execute(event))

    }

}