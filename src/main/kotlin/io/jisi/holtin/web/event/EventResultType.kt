package io.jisi.holtin.web.event

enum class EventResultType {

    PASS,//正常内容，建议直接放行
    REVIEW,//可疑内容，建议人工审核
    REJECT,//违规内容，建议直接拦截
}