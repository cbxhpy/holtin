package io.jisi.holtin.web.event

import org.slf4j.LoggerFactory
import java.util.*

/**
 * Created by sunpeak on 2016/8/6.
 */
abstract class Event {

    private val logger = LoggerFactory.getLogger(Event::class.java)

    /**
     * 场景
     */
    var scene: String? = null

    /**
     * 事件id
     */
    var id: String? = null

    /**
     * 操作时间
     */
    var operateTime: Date? = null

    /**
     * 事件评分
     */
    var score: Int = 0

    var result: EventResultType = EventResultType.PASS

    private var index = 1
    var log = mutableMapOf<Int, TaskLog>()

    fun executeLog(fileName: String, result: Boolean, desc: String) {
        log[index++] = TaskLog(fileName, result, desc)
    }

    class TaskLog(
            val fileName: String,
            val result: Boolean,
            val desc: String
    )
}
