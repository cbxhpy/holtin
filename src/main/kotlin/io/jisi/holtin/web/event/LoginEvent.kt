package io.jisi.holtin.web.event

/**
 * Created by sunpeak on 2016/8/6.
 */
class LoginEvent : Event() {

    var mobile: String? = null

    var operateIp: String? = null

    companion object {

        val MOBILE = "mobile"

        val OPERATEIP = "operateIp"
    }

}
