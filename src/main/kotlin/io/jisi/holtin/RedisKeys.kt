package io.jisi.holtin

/**
 * @author obaw
 * @time 2019-07-10 09:38
 * @describe
 */
class RedisKey {

    private val prefix: String

    constructor(prefix: String) {
        this.prefix = "$prefix:"
    }

    /**
     * 生成 redis key, 采用`:`*分隔。
     */
    fun key(vararg segments: Any) = segments.joinToString(prefix = prefix, separator = ":")
}

object RedisKeys {
    object BlackList {
        val ip = RedisKey("blacklist:ip")
        val deviceid = RedisKey("blacklist:deviceid")
    }

    object Threshold {
        val ip = RedisKey("threshold:ip")
        val deviceid = RedisKey("blacklist:deviceid")
    }
}