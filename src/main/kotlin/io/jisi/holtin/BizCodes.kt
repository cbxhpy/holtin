package io.jisi.holtin

import io.zhudy.kitty.biz.BizCode

/**
 * 业务错误码。
 *
 * @author Kevin Zou (kevinz@weghst.com)
 */
enum class BizCodes(override val code: Int, override val msg: String, override val status: Int = 400) : BizCode {

    // ====================================================================//
    // 3000 - 3100 管理员错误码
    // ====================================================================//
    C_3000(3000, "登录密码不匹配"),


}