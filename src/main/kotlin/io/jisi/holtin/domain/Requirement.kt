package io.jisi.holtin.domain

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
enum class Requirement(val title: String, val group: String) {
    ADMIN_ADD("新增管理员", "admin"),
    ADMIN_LIST("管理员列表", "admin"),
    ROLE_ADD("新增角色", "role"),
    ROLE_EDIT("编辑角色", "role"),


}