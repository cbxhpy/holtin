package io.jisi.holtin.domain

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
data class PermissionRequest(
    val requirement: Requirement,
    val method: String,
    val url: String
)