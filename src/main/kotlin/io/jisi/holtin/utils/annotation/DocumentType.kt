package io.jisi.holtin.utils.annotation

import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.type.filter.AnnotationTypeFilter
import org.springframework.data.convert.TypeInformationMapper
import org.springframework.data.mapping.Alias
import org.springframework.data.util.ClassTypeInformation
import org.springframework.data.util.TypeInformation

/**
 * @author obaw
 * @time 2019-08-04 17:03
 * @describe
 */
@Retention
@Target(AnnotationTarget.CLASS)
annotation class DocumentType(val value: String = "")

/**
 * @author obaw
 * @time 2019-08-04 16:57
 * @describe mongo 类型转换
 */
class AnnotationTypeInformationMapper(basePackagesToScan: List<String>) : TypeInformationMapper {

    private val typeToAliasMap = mutableMapOf<ClassTypeInformation<*>, Alias>()

    private val aliasToTypeMap = mutableMapOf<Alias, ClassTypeInformation<*>>()

    init {
        populateTypeMap(basePackagesToScan)
    }

    private fun populateTypeMap(basePackagesToScan: List<String>) {
        val scanner = ClassPathScanningCandidateComponentProvider(false)

        scanner.addIncludeFilter(AnnotationTypeFilter(DocumentType::class.java))

        for (basePackage in basePackagesToScan) {
            for (bd in scanner.findCandidateComponents(basePackage)) {
                try {
                    val clazz = Class.forName(bd.beanClassName)
                    val documentTypeAnnotation = clazz.getAnnotation(DocumentType::class.java)

                    val type = ClassTypeInformation.from(clazz)
                    val alias = documentTypeAnnotation.value

                    typeToAliasMap[type] = Alias.of(alias)
                    aliasToTypeMap[Alias.of(alias)] = type

                } catch (e: ClassNotFoundException) {
                    throw IllegalStateException(String.format("Class [%s] could not be loaded.", bd.beanClassName), e)
                }

            }
        }
    }

    override fun createAliasFor(type: TypeInformation<*>): Alias {
        return typeToAliasMap[type] ?: Alias.NONE
    }

    override fun resolveTypeFrom(alias: Alias): ClassTypeInformation<*>? {
        return aliasToTypeMap[alias]
    }

    class Builder {

        private var basePackagesToScan = mutableListOf<String>()

        fun withBasePackage(basePackage: String): Builder {
            basePackagesToScan.add(basePackage)
            return this
        }

        fun withBasePackages(basePackages: List<String>): Builder {
            basePackagesToScan.addAll(basePackages)
            return this
        }

        fun withBasePackages(basePackages: Collection<String>): Builder {
            basePackagesToScan.addAll(basePackages)
            return this
        }

        fun build(): AnnotationTypeInformationMapper {
            return AnnotationTypeInformationMapper(basePackagesToScan)
        }
    }
}