package io.jisi.holtin.utils

import io.lettuce.core.api.sync.RedisCommands
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils

@Component
class RedisLock(
    val redisTemplate: RedisCommands<String, String>

) {
    val logger = LoggerFactory.getLogger(RedisLock::class.java)

    /**
     * 加锁
     * @param key 加锁的key
     * @param timeout 超时时间，默认 2000ms
     * 当前线程操作时的 System.currentTimeMillis() + timeout，timeout是超时时间，这个地方不需要去设置redis的expire，
     * 也不需要超时后手动去删除该key，因为会存在并发的线程都会去删除，造成上一个锁失效，结果都获得锁去执行，并发操作失败了就。
     * @return
     */
    fun lock(key: String, timeout: Long = 2000): Boolean {
        val value = (System.currentTimeMillis() + timeout).toString()
        //如果key值不存在，则返回 true，且设置 value
        if (redisTemplate.setnx(key, value)) {
            return true
        }
        //获取key的值，判断是是否超时
        val curVal = redisTemplate.get(key)
        if (curVal.isNotEmpty() && curVal.toLong() < System.currentTimeMillis()) {
            //获得之前的key值，同时设置当前的传入的value。这个地方可能几个线程同时过来，但是redis本身天然是单线程的，所以getAndSet方法还是会安全执行，
            //首先执行的线程，此时curVal当然和oldVal值相等，因为就是同一个值，之后该线程set了自己的value，后面的线程就取不到锁了
            val oldVal = redisTemplate.getset(key, value)
            if (oldVal.isNotEmpty() && oldVal == curVal) {
                return true
            }
        }
        return false
    }

    /**
     * 解锁
     * @param key
     * @param value
     */
    fun unlock(key: String) {
        try {
            val curVal = redisTemplate.get(key)
            if (curVal.isNotEmpty()) {
                redisTemplate.del(key)
            }
        } catch (e: Exception) {
            logger.error("解锁异常")
        }

    }
}