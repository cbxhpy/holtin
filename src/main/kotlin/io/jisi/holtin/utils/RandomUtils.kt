package io.jisi.holtin.utils

import java.util.*

/**
 * @author obaw
 * @time 2019-08-04 14:27
 * @describe
 */
object RandomUtils {

    fun stringId() = UUID.randomUUID().toString().replace("-", "")

}