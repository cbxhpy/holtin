package io.jisi.holtin.props

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import java.util.concurrent.TimeUnit

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Configuration
@ConfigurationProperties("roar-admin")
class AdminProps {

    /**
     * 登录 JWT 配置。
     */
    val jwt = Jwt()
    /**
     * 超级用户。
     */
    val rootUser = RootUser()


    class RootUser {

        /**
         * 姓名。
         */
        var name = ""
        /**
         * 登录邮箱。
         */
        var email = ""
        /**
         * 登录密码。
         */
        var password = ""
        /**
         * 是否自动创建。
         */
        var autoCreate = false
    }

    class Jwt {
        /**
         * 密钥。
         */
        var secret = "aa075d9328a87165d0327c6bee50e27a800d759d5fd813ba02cb21c2a37abd00e2ad94ce61138ce89b73ad4645d86654"
        /**
         * 过期时间。
         */
        var expiresIn = TimeUnit.DAYS.toSeconds(1)
    }

}