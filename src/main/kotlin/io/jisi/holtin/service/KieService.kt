package io.jisi.holtin.service

import io.jisi.holtin.web.event.Event
import io.jisi.holtin.web.event.LoginEvent
import org.kie.api.KieBase
import org.kie.api.io.ResourceType
import org.kie.api.runtime.KieSession
import org.kie.api.runtime.StatelessKieSession
import org.kie.internal.KnowledgeBase
import org.kie.internal.KnowledgeBaseFactory
import org.kie.internal.builder.KnowledgeBuilder
import org.kie.internal.builder.KnowledgeBuilderFactory
import org.kie.internal.io.ResourceFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.io.File
import java.net.URISyntaxException

@Service
class KieService(
        private val kieSession: KieSession,
        private val kbase: KieBase,
        private val blackListService: BlackListService,
        private val thresholdService: ThresholdService

) {
    private val logger = LoggerFactory.getLogger(KieService::class.java)


    /**
     * 规则引擎执行
     *
     * @param object
     */
    fun execute(event: LoginEvent): Map<String, Any> {
        kieSession.setGlobal("blackListService", blackListService)
        kieSession.setGlobal("thresholdService", thresholdService)
        kieSession.insert(event)
        val ruleFiredCount = kieSession.fireAllRules()
        println("触发了" + ruleFiredCount + "条规则");
        println(event)
        return mapOf<String, Any>(
                "result" to event.result,
                "score" to event.score
        )

    }


}
