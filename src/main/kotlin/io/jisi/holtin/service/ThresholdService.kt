package io.jisi.holtin.service

import io.jisi.holtin.RedisKeys
import io.lettuce.core.ScriptOutputType
import io.lettuce.core.api.sync.RedisCommands
import org.springframework.stereotype.Service

@Service
class ThresholdService(
        private val coreRedis: RedisCommands<String, String>) {

    private val thresholdIp = javaClass.getResource("/redis/threshold_ip.lua").readText()

    fun ip(ip: String, limit: Int, timeout: Long) = coreRedis.eval<Boolean>(thresholdIp, ScriptOutputType.BOOLEAN,
            arrayOf(RedisKeys.Threshold.ip.key(ip)), System.currentTimeMillis().toString(), limit.toString(), timeout.toString())


}