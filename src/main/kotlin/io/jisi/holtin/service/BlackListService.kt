package io.jisi.holtin.service

import io.jisi.holtin.RedisKeys
import io.lettuce.core.api.sync.RedisCommands
import org.springframework.stereotype.Service

@Service
class BlackListService(
        private val coreRedis: RedisCommands<String, String>
) {

    fun ip(ip: String) = coreRedis.sismember(RedisKeys.BlackList.ip.key(), ip)

}