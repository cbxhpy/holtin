package io.jisi.holtin.service

import io.zhudy.kitty.auth.UserContext
import io.jisi.holtin.dao.AdminDao
import io.jisi.holtin.dao.RoleDao
import io.jisi.holtin.domain.PermissionRequest
import io.jisi.holtin.domain.Requirement
import io.jisi.holtin.dto.PermissionDto
import io.jisi.holtin.pojo.Role
import org.springframework.stereotype.Service

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Service
class RoleService(
    private val adminDao: AdminDao,
    private val roleDao: RoleDao
) {

    companion object {
        /**
         * 超级角色，拥有系统所有权限。
         */
        const val SUPER_ROLE = "ROOT"
    }

    /**
     * 校验用户是否有指定的权限。
     */
    fun checkPermission(request: PermissionRequest, uc: UserContext): Boolean {
        val admin = adminDao.findById(uc.id.toLong())
        if (admin.roles.isEmpty()) {
            return false
        }
        if (admin.roles.contains(SUPER_ROLE)) {
            return true
        }

        val roles = roleDao.findRoles(admin.roles)
        for (r in roles) {
            if (r.perms.contains(request.requirement.name)) {
                return true
            }
        }
        return false
    }

    /**
     * 系统权限列表。
     */
    fun permissions() = Requirement.values().map { PermissionDto(it.name, it.title, it.group) }

    /**
     * 保存角色。
     */
    fun save(role: Role) = roleDao.save(role)

    /**
     * 更新角色。
     */
    fun update(role: Role) = roleDao.update(role)

    /**
     * 查询系统所有角色。
     */
    fun findAll() = roleDao.findAll()
}