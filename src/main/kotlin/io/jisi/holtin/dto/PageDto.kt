package io.jisi.holtin.dto

import com.fasterxml.jackson.annotation.JsonIgnore
import io.jisi.holtin.web.qo.PageQo
import kotlin.math.ceil


/**
 * @author obaw
 * @time 2019-08-19 15:40
 * @describe
 */
data class PageDto<out T>(
    val items: List<T> = emptyList(),
    val totalItems: Long = 0L,
    @JsonIgnore
    val pageable: PageQo
) {
    val page get() = pageable.page

    val size get() = pageable.size

    val totalPage = ceil(totalItems.toDouble() / pageable.size.toDouble()).toInt()
}