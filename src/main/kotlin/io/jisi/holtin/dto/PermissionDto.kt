package io.jisi.holtin.dto

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
data class PermissionDto(
    val name: String,
    val title: String,
    val group: String
)