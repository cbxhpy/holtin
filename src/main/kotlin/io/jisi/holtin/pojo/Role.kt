package io.jisi.holtin.pojo

import org.joda.time.LocalDateTime
import org.springframework.data.mongodb.core.mapping.Document

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Document
data class Role(
    var id: String = "",
    var name: String = "",
    var perms: List<String> = emptyList(),
    var time: LocalDateTime = LocalDateTime.now()
)