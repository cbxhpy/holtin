package io.jisi.holtin.pojo

import org.joda.time.LocalDateTime
import org.springframework.data.mongodb.core.mapping.Document

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Document(collection = "admins")
data class Admin(
    var id: Long = 0,
    var name: String = "",
    var email: String = "",
    var password: String = "",
    var roles: List<String> = emptyList(),
    var disabled: Boolean = false,
    var time: LocalDateTime = LocalDateTime.now()
)