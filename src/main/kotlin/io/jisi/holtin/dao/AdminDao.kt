package io.jisi.holtin.dao

import io.jisi.holtin.dto.PageDto
import io.jisi.holtin.pojo.Admin
import io.jisi.holtin.web.qo.PageQo
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Repository

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Repository
class AdminDao(
    private val adminMongo: MongoTemplate,
    private val idHelper: UniqueIdHelper
) {

    fun save(admin: Admin) {
        admin.id = idHelper.nextId("admins_id")
        adminMongo.insert(admin)
    }

    fun findById(id: Long): Admin {
        return adminMongo.findById(id, Admin::class.java)
    }

    /**
     * 根据 email 查询管理员信息。
     */
    fun findByEmail(email: String): Admin? =
        adminMongo.findOne(Query.query(Criteria.where("email").`is`(email)), Admin::class.java)

    /**
     * 查询管理员列表。
     */
    fun find(pageQo: PageQo): PageDto<Admin> {
        val query = Query()
        query.fields().exclude("password")
        val total = adminMongo.count(query, Admin::class.java)
        if (total == 0L) {
            return PageDto(emptyList(), total, pageQo)
        }
        val items = adminMongo.find(query.with(PageRequest.of(pageQo.qPage, pageQo.size)), Admin::class.java)
        return PageDto(items, total, pageQo)
    }
}