package io.jisi.holtin.dao

import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.FindOneAndUpdateOptions
import com.mongodb.client.model.ReturnDocument
import com.mongodb.client.model.Updates.inc
import org.bson.Document
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Component
class UniqueIdHelper(
    adminMongo: MongoTemplate
) {

    private val coll = adminMongo.getCollection("kgs")

    /**
     * 设置初始值。
     */
    fun initVal(field: String, init: Long) {
        if (coll.countDocuments(eq("_id", field)) == 0L) {
            coll.insertOne(
                Document().append("_id", field).append("seq", init)
            )
        }
    }

    /**
     * 获取下一个ID。
     */
    fun nextId(field: String): Long {
        val d = coll.findOneAndUpdate(
            eq("_id", field),
            inc("seq", 1L),
            FindOneAndUpdateOptions().returnDocument(ReturnDocument.AFTER).upsert(true)
        )
        return d.getLong("seq")
    }
}