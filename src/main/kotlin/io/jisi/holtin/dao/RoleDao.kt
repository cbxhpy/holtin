package io.jisi.holtin.dao

import io.jisi.holtin.pojo.Role
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Repository
class RoleDao(
    private val adminMongo: MongoTemplate
) {

    /**
     * 保存角色。
     */
    fun save(role: Role) {
        adminMongo.insert(role)
    }

    /**
     * 修改角色。
     */
    fun update(role: Role) {
        adminMongo.updateFirst(
            Query.query(Criteria.where("_id").`is`(role.id)),
            Update()
                .set("name", role.name)
                .set("perms", role.perms),
            Role::class.java
        )
    }

    /**
     * 查询角色。
     */
    fun findRoles(ids: List<String>): List<Role> {
        return adminMongo.find(Query.query(Criteria.where("_id").`in`(ids)), Role::class.java)
    }

    /**
     * 查询系统所有角色。
     */
    fun findAll(): List<Role> = adminMongo.findAll(Role::class.java)
}