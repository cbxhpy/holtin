package io.jisi.holtin.config

import org.kie.api.KieBase
import org.kie.api.KieServices
import org.kie.api.builder.KieFileSystem
import org.kie.api.runtime.KieContainer
import org.kie.api.runtime.KieSession
import org.kie.internal.io.ResourceFactory
import org.kie.spring.KModuleBeanFactoryPostProcessor
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.Resource
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import java.io.IOException

@Configuration
class DroolsAutoConfiguration {

    private val RULES_PATH = "rules/"
    /**
     * 这里要引入 org.springframework.core.io.Resource  包
     *
     * @return
     * @throws IOException
     */
    private val ruleFiles: Array<Resource>
        @Throws(IOException::class)
        get() {
            val resourcePatternResolver = PathMatchingResourcePatternResolver()
            return resourcePatternResolver.getResources("classpath*:$RULES_PATH**/*.*")
        }

    private val kieServices: KieServices
        get() = KieServices.Factory.get()

    @Bean
    @ConditionalOnMissingBean(KieFileSystem::class)
    @Throws(IOException::class)
    fun kieFileSystem(): KieFileSystem {
        val kieFileSystem = kieServices.newKieFileSystem()
        for (file in ruleFiles) {
            val path = RULES_PATH + file.filename
            kieFileSystem.write(ResourceFactory.newClassPathResource(path, "UTF-8"))
        }
        return kieFileSystem
    }

    @Bean
    @ConditionalOnMissingBean(KieContainer::class)
    @Throws(IOException::class)
    fun kieContainer(): KieContainer {
        val kieRepository = kieServices.repository

        kieRepository.addKieModule { kieRepository.defaultReleaseId }

        val kieBuilder = kieServices.newKieBuilder(kieFileSystem())
        kieBuilder.buildAll()

        return kieServices.newKieContainer(kieRepository.defaultReleaseId)
    }

    @Bean
    @ConditionalOnMissingBean(KieBase::class)
    @Throws(IOException::class)
    fun kieBase(): KieBase {
        return kieContainer().kieBase
    }

    @Bean
    @ConditionalOnMissingBean(KieSession::class)
    @Throws(IOException::class)
    fun kieSession(): KieSession {
        return kieContainer().newKieSession()
    }

    @Bean
    @ConditionalOnMissingBean(KModuleBeanFactoryPostProcessor::class)
    fun kiePostProcessor(): KModuleBeanFactoryPostProcessor {
        return KModuleBeanFactoryPostProcessor()
    }

}

