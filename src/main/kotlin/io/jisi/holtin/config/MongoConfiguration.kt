package io.jisi.holtin.config

import com.mongodb.MongoClient
import com.mongodb.MongoClientOptions
import com.mongodb.ReadPreference
import com.mongodb.WriteConcern
import io.jisi.holtin.utils.annotation.AnnotationTypeInformationMapper
import org.springframework.boot.autoconfigure.mongo.MongoClientFactory
import org.springframework.boot.autoconfigure.mongo.MongoProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.data.mongodb.MongoDbFactory
import org.springframework.data.mongodb.MongoTransactionManager
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.SimpleMongoDbFactory
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper
import org.springframework.data.mongodb.core.convert.MappingMongoConverter
import org.springframework.data.mongodb.core.convert.MongoCustomConversions
import org.springframework.data.mongodb.core.mapping.MongoMappingContext
import org.springframework.data.transaction.ChainedTransactionManager
import org.springframework.transaction.PlatformTransactionManager
import java.util.concurrent.TimeUnit

/**
 * MongoDB 数据库配置。
 *
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Configuration(proxyBeanMethods = false)
class MongoConfiguration {

    @Bean
    @ConfigurationProperties("database.mongo")
    fun coreProperties() = MongoProperties()

    @Bean(destroyMethod = "close")
    fun coreClient(coreProperties: MongoProperties, environment: Environment): MongoClient {
        val opts = MongoClientOptions.builder()
            .connectTimeout(2000)
            .socketTimeout(3000)
            .minConnectionsPerHost(60)
            .writeConcern(WriteConcern.W2)
            .maxConnectionIdleTime(TimeUnit.MINUTES.toMillis(1).toInt())
            .readPreference(ReadPreference.primary())
            .build()

        return MongoClientFactory(coreProperties, environment).createMongoClient(opts)
    }

    @Bean
    fun coreMongoDbFactory(coreClient: MongoClient): MongoDbFactory {
        return SimpleMongoDbFactory(coreClient, "core")
    }

    @Bean
    fun coreMongo(coreMongoDbFactory: MongoDbFactory): MongoTemplate {
        return MongoTemplate(coreMongoDbFactory, mappingMongoConverter(coreMongoDbFactory))
    }

    @Bean
    fun logMongoDbFactory(coreClient: MongoClient): MongoDbFactory {
        return SimpleMongoDbFactory(coreClient, "logs")
    }

    @Bean
    fun logMongo(logMongoDbFactory: MongoDbFactory): MongoTemplate {
        return MongoTemplate(logMongoDbFactory, mappingMongoConverter(logMongoDbFactory))
    }

    @Bean
    fun adminMongoDbFactory(coreClient: MongoClient): MongoDbFactory {
        return SimpleMongoDbFactory(coreClient, "_admin")
    }

    @Bean
    fun adminMongo(adminMongoDbFactory: MongoDbFactory): MongoTemplate {
        return MongoTemplate(adminMongoDbFactory, mappingMongoConverter(adminMongoDbFactory))
    }

    @Bean
    fun ttlLogMongoDbFactory(coreClient: MongoClient): MongoDbFactory {
        return SimpleMongoDbFactory(coreClient, "ttl_logs")
    }

    @Bean
    fun ttlLogMongo(ttlLogMongoDbFactory: MongoDbFactory): MongoTemplate {
        return MongoTemplate(ttlLogMongoDbFactory, mappingMongoConverter(ttlLogMongoDbFactory))
    }

    @Bean
    fun transactionManager(
        coreMongoDbFactory: MongoDbFactory,
        logMongoDbFactory: MongoDbFactory,
        adminMongoDbFactory: MongoDbFactory,
        ttlLogMongoDbFactory: MongoDbFactory
    ): PlatformTransactionManager {
        return ChainedTransactionManager(
            MongoTransactionManager(coreMongoDbFactory),
            MongoTransactionManager(logMongoDbFactory),
            MongoTransactionManager(adminMongoDbFactory),
            MongoTransactionManager(ttlLogMongoDbFactory)
        )
    }

    fun mappingMongoConverter(factory: MongoDbFactory): MappingMongoConverter {
        val dbRefResolver = DefaultDbRefResolver(factory)

        val conversions = MongoCustomConversions(emptyList<Any>())

        val mappingContext = MongoMappingContext()
        mappingContext.setSimpleTypeHolder(conversions.simpleTypeHolder)
        mappingContext.isAutoIndexCreation = false
        mappingContext.afterPropertiesSet()

        val converter = MappingMongoConverter(dbRefResolver, mappingContext)
        converter.setCustomConversions(conversions)
        // 保留一些对象的_class字段，方便转换，如Any
        converter.typeMapper = DefaultMongoTypeMapper(
            DefaultMongoTypeMapper.DEFAULT_TYPE_KEY,
            listOf(
                AnnotationTypeInformationMapper.Builder().withBasePackage("io.jisi.holtin.pojo").build()
            )
        )
        converter.afterPropertiesSet()

        return converter
    }
}