package io.jisi.holtin.config

import com.auth0.jwt.algorithms.Algorithm
import io.zhudy.kitty.annotation.NoIntegrationTest
import io.jisi.holtin.props.AdminProps
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.context.support.BeanDefinitionDsl
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.client.RestTemplate

/**
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Configuration(proxyBeanMethods = false)
class DefaultConfiguration {

    companion object {

        // BeanFactoryPostProcessor 采用静态方式初始化
        @Bean
        fun kotlinPropertyConfigurer() = PropertySourcesPlaceholderConfigurer().apply {
            setPlaceholderPrefix("%{")
            setTrimValues(true)
            setIgnoreUnresolvablePlaceholders(true)
        }
    }

    @Bean
    fun passwordEncoder() = BCryptPasswordEncoder()

    @Bean
    fun jwtAlgorithm(adminProps: AdminProps): Algorithm = Algorithm.HMAC256(adminProps.jwt.secret)

    @Bean
    @NoIntegrationTest
    fun clientHttpRequestFactory() = OkHttp3ClientHttpRequestFactory().apply {
        setConnectTimeout(5)
        setReadTimeout(30)
        setWriteTimeout(30)
    }

    @Bean
    @NoIntegrationTest
    fun restTemplate(
        restTemplateBuilder: RestTemplateBuilder,
        clientHttpRequestFactory: ClientHttpRequestFactory
    ): RestTemplate = restTemplateBuilder.requestFactory { clientHttpRequestFactory }.build()
}