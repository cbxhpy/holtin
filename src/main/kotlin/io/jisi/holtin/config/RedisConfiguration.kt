package io.jisi.holtin.config

import io.lettuce.core.RedisClient
import io.lettuce.core.api.StatefulRedisConnection
import io.lettuce.core.api.async.RedisAsyncCommands
import io.lettuce.core.api.sync.RedisCommands
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 *
 * @author Kevin Zou (kevinz@weghst.com)
 */
@Configuration(proxyBeanMethods = false)
class RedisConfiguration {
    @Bean(destroyMethod = "shutdown")
    fun coreRedisClient(@Value("%{database.redis.core-uri}") uri: String) = RedisClient.create(uri)!!

    @Bean(destroyMethod = "close")
    fun coreRedisConn(coreRedisClient: RedisClient) = coreRedisClient.connect()!!

    @Bean
    fun coreRedis(coreRedisConn: StatefulRedisConnection<String, String>): RedisCommands<String, String> {
        return coreRedisConn.sync()
    }

    @Bean
    fun coreRedisAsync(coreRedisConn: StatefulRedisConnection<String, String>): RedisAsyncCommands<String, String> {
        return coreRedisConn.async()
    }
}