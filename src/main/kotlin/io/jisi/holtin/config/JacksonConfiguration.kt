package io.jisi.holtin.config

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.datatype.joda.cfg.JacksonJodaDateFormat
import com.fasterxml.jackson.datatype.joda.deser.LocalDateDeserializer
import com.fasterxml.jackson.datatype.joda.deser.LocalDateTimeDeserializer
import com.fasterxml.jackson.datatype.joda.ser.LocalDateSerializer
import com.fasterxml.jackson.datatype.joda.ser.LocalDateTimeSerializer
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.joda.time.format.DateTimeFormat
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

/**
 * @author obaw
 * @time 2019-08-08 16:21
 * @describe
 */
@Configuration(proxyBeanMethods = false)
class JacksonConfiguration {

    @Bean
    fun objectMapper(): ObjectMapper {
        return Jackson2ObjectMapperBuilder.json()
            .serializationInclusion(JsonInclude.Include.NON_NULL)
            .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .modules(JavaTimeModule(), KotlinModule(), JodaModule(), Jdk8Module())
            .serializers(
                LocalDateSerializer(JacksonJodaDateFormat(DateTimeFormat.forPattern("yyyy-MM-dd 00:00:00"))),
                LocalDateTimeSerializer(JacksonJodaDateFormat(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")))
            )
            .deserializers(
                LocalDateTimeDeserializer(JacksonJodaDateFormat(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")))
            )
            .propertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
            .build()
    }

}