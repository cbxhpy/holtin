package io.jisi.holtin.service

import io.jisi.holtin.server.Main
import io.jisi.holtin.web.event.LoginEvent
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Main::class])
class KieServiceTest {
    @Autowired
    private lateinit var kieService: KieService

    @Test
    fun testExecute() {
        val event = LoginEvent()
        event.operateIp = "192.168.1.1"

        kieService.execute(event)

    }
}